package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;

@Component
public abstract class AbstractTaskShowCommand extends AbstractTaskCommand {

    public void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}