package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-start-by-id";

    @NotNull
    private final String DESCRIPTION = "Start project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(getToken(), id, Status.IN_PROGRESS);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}