package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskListByProjectIdCommand extends AbstractTaskListCommand {

    @NotNull
    private final String NAME = "task-show-by-project-id";

    @NotNull
    private final String DESCRIPTION = "Show task list by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken(), projectId);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}