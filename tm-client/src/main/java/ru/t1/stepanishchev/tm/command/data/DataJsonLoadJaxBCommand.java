package ru.t1.stepanishchev.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.DataJsonLoadJaxBRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;

@Component
public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-load-json-jaxb";

    @Getter
    @NotNull
    private final String description = "Load data from json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        domainEndpoint.loadDataJsonJaxb(new DataJsonLoadJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}