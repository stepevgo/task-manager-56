package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.endpoint.ITaskEndpoint;
import ru.t1.stepanishchev.tm.command.AbstractCommand;
import ru.t1.stepanishchev.tm.enumerated.Role;

@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}