package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.ICommandRepository;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

import java.util.Collection;

@Service
public interface ICommandService extends ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

}