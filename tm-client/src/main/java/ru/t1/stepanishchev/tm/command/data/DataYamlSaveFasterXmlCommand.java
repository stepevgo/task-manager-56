package ru.t1.stepanishchev.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.t1.stepanishchev.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;

@Component
public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-yaml";

    @Getter
    @NotNull
    private final String description = "Save data in yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        domainEndpoint.saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}