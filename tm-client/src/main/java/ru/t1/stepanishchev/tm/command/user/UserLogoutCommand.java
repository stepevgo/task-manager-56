package ru.t1.stepanishchev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.UserLogoutRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";

    @NotNull
    private final String DESCRIPTION = "logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        request.setToken(getToken());
        authEndpoint.logout(request);
        setToken(null);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}