package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.TaskShowByIdRequest;
import ru.t1.stepanishchev.tm.dto.response.TaskShowByIdResponse;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdCommand extends AbstractTaskShowCommand {

    @NotNull
    private final String NAME = "task-show-by-id";

    @NotNull
    private final String DESCRIPTION = "Display task by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken(), id);
        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        showTask(response.getTask());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}