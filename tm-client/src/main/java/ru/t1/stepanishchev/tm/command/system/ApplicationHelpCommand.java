package ru.t1.stepanishchev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.model.ICommand;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    private final String DESCRIPTION = "Show list terminal commands.";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (@Nullable final ICommand command : commands) {
            if (command == null) continue;
            System.out.println(command.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}