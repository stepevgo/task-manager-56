package ru.t1.stepanishchev.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;
import ru.t1.stepanishchev.tm.model.Project;
import ru.t1.stepanishchev.tm.model.Session;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@ComponentScan("ru.t1.stepanishchev.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl());
        settings.put(Environment.USER, propertyService.getDBUser());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDBShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getDBFormatSql());
        settings.put(Environment.USE_SQL_COMMENTS, propertyService.getDBCommentsSql());

        settings.put(USE_SECOND_LEVEL_CACHE, propertyService.getDBSecondLvlCash());
        settings.put(CACHE_REGION_FACTORY, propertyService.getDBFactoryClass());
        settings.put(USE_QUERY_CACHE, propertyService.getDBUseQueryCash());
        settings.put(USE_MINIMAL_PUTS, propertyService.getDBUseMinPuts());
        settings.put(CACHE_REGION_PREFIX, propertyService.getDBRegionPrefix());
        settings.put(CACHE_PROVIDER_CONFIG, propertyService.getDBHazelConfig());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}

