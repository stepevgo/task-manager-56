package ru.t1.stepanishchev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "34121";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "3322243";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "12412312";

    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String DATABASE_USER_NAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USER_NAME_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_USER_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_USER_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://localhost:5432/TM";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_COMMENT_SQL = "database.comment_sql";

    @NotNull
    private static final String DATABASE_COMMENT_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CASH = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_USE_QUERY_CASH = "database.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_MINIMAL_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_HAZEL_CONFIG = "database.config_file_path";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    private String getEnvKey(@NotNull final String key) {
        return key.replace(".","_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(key);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DATABASE_USER_NAME_KEY, DATABASE_USER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DATABASE_USER_PASSWORD_KEY, DATABASE_USER_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @Override
    @NotNull
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, DATABASE_FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCommentsSql() {
        return getStringValue(DATABASE_COMMENT_SQL, DATABASE_COMMENT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBSecondLvlCash() {
        return getStringValue(DATABASE_SECOND_LVL_CASH);
    }

    @NotNull
    @Override
    public String getDBFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS);
    }

    @NotNull
    @Override
    public String getDBUseQueryCash() {
        return getStringValue(DATABASE_USE_QUERY_CASH);
    }

    @NotNull
    @Override
    public String getDBUseMinPuts() {
        return getStringValue(DATABASE_USE_MINIMAL_PUTS);
    }

    @NotNull
    @Override
    public String getDBRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX);
    }

    @NotNull
    @Override
    public String getDBHazelConfig() {
        return getStringValue(DATABASE_HAZEL_CONFIG);
    }

}

