package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.component.ISaltProvider;
import ru.t1.stepanishchev.tm.api.property.IDatabaseProperty;
public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBCommentsSql();

    @NotNull
    String getDBSecondLvlCash();

    @NotNull
    String getDBFactoryClass();

    @NotNull
    String getDBUseQueryCash();

    @NotNull
    String getDBUseMinPuts();

    @NotNull
    String getDBRegionPrefix();

    @NotNull
    String getDBHazelConfig();

}
