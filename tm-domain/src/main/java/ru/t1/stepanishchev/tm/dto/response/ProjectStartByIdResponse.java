package ru.t1.stepanishchev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}